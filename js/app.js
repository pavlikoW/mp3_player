$("document").ready(function(){
  var songrdy = false;
  var count = -1;
  var activenum = 0;
  var output = "";
  var songsources = [];
  var picturesources = [];
  var titles = [];
  var authors = [];
  var descriptions = [];
  var durations = [];
  // Functions
  function formatTime(seconds) {
    minutes = Math.floor(seconds / 60);
    minutes = (minutes >= 10) ? minutes : "0" + minutes;
    seconds = Math.floor(seconds % 60);
    seconds = (seconds >= 10) ? seconds : "0" + seconds;
    return minutes + ":" + seconds;
  }
  function readURL(input,image) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      $(image).attr("src", e.target.result);
      if(image=="#song"){
        songsources.push(e.target.result);
      }else{picturesources.push(e.target.result);}
    }

    reader.readAsDataURL(input.files[0]);
    }
  }
// Controlers
  $("#set-cover").change(function() {
    readURL(this,"#song-image");
  });
  $("#set-song").change(function() {
    readURL(this,"#song");
  });
  $(".play").click(function(){
    if (songrdy == true){
        var $this = $(this),
            song = $this.siblings("audio")[0];
        if (song.paused){
          song.play();
          $this.attr("src","./img/pause.png");
          $("#tot-time").text(formatTime(song.duration));
          setInterval(function(){
          $("#cur-time").text(formatTime(song.currentTime))},1000);
          setInterval(function(){
            var dotpos= song.currentTime/song.duration*100+"%";
            $("#progressdot").css("margin-left",dotpos)},1000);
        }
        else{
          song.pause();
          $this.attr("src","./img/play.png");
        }
    }
});

  $(".forward").click(function(){
    song.currentTime= song.currentTime+15;
  });
  $(".backward").click(function(){
    if(song.currentTime<=15) {
      song.currentTime = 0;
    }else {
      song.currentTime= song.currentTime-15;
    }
  });
  $(".next").click(function(){
    if (activenum > count-0.5){
      activenum = 0;
    }else{
      activenum +=1;
    };

    $(".active").removeClass("active").addClass("activated");
    if ($(".activated").is(":last-child")){
      $(".item").first().addClass("active");
    } else {
      $(".activated").next().addClass("active");
    }
    $(".activated").removeClass("activated");

    $("audio").attr("src",songsources[activenum]);
    $("song-image").attr("src",picturesources[activenum]);
    $("#song-title").text(titles[activenum]);
    $("#song-author").text(authors[activenum]);
    $("song-description").text(descriptions[activenum]);
  });

  $(".previous").click(function(){
    if (activenum < 0.5){
      activenum = count;
    }else{
      activenum -=1;
    };

    $(".active").removeClass("active").addClass("activated");
    if ($(".activated").is(":first-child")){
      $(".item").last().addClass("active");
    } else {
      $(".activated").prev().addClass("active");
    }
    $(".activated").removeClass("activated");

    $("audio").attr("src",songsources[activenum]);
    $("song-image").attr("src",picturesources[activenum]);
    $("#song-title").text(titles[activenum]);
    $("#song-author").text(authors[activenum]);
    $("song-description").text(descriptions[activenum]);
  });


  $("#progressbar").click(function(e) {
    var newtime = e.pageX - $("#progressbar").offset().left;
    newtime = newtime/$("#progressbar").width() *100;
    $("#progressdot").css("margin-left",newtime);
    newtime = newtime/100 * song.duration;
    song.currentTime=newtime;
  });
  function delaybutton(){
    $("#save-button").toggleClass("btn-danger")
  }
  $("#set-song").click(function(){
    $("#save-button").addClass("btn-danger")
    setTimeout(delaybutton,6000);
  })
  $("#save-button").click(function(){
    var title = $("#set-title").val();
    var author = $("#set-author").val();
    var description = $("#set-desc").val();
    var mp3 = $("#set-song").val();
    var duration = formatTime(song.duration);
    output += `<p class="item">${title}  -  ${author}<span>${duration}</span></p>`
    if (title !="" && mp3 !=""){// if(title === true && song === true){
      $("#song-title").text(title);
      $("#song-author").text(author);
      $("#song-description").text(description);
      songrdy = true;
      $("#song-list").html(output);
      $("#song-list p").last().addClass("active");
      $("#song-list p").last().prev().removeClass("active");
      count+=1;
      activenum = count;


      titles.push(title);
      authors.push(author);
      descriptions.push(description);
      durations.push(duration);
      }
  });

});
